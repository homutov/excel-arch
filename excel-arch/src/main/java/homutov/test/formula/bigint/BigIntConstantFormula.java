package homutov.test.formula.bigint;

import homutov.test.formula.Formula;
import homutov.test.formula.Range;
import homutov.test.sheet.CellIndex;

import java.math.BigInteger;
import java.util.Collections;
import java.util.List;
import java.util.Map;

class BigIntConstantFormula implements Formula<BigInteger> {

    private final BigInteger value;

    BigIntConstantFormula(BigInteger value) {
        this.value = value;
    }

    @Override
    public boolean isConstant() {
        return true;
    }

    @Override
    public BigInteger calculate(Map<CellIndex, BigInteger> values) {
        return value;
    }

    @Override
    public List<Range> getDependencies() {
        return Collections.emptyList();
    }
}
