package homutov.test.formula.bigint;

import homutov.test.formula.Formula;
import homutov.test.formula.Range;
import homutov.test.sheet.CellIndex;

import java.math.BigInteger;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class BigIntCellFormula implements Formula<BigInteger> {

    private final CellIndex cell;

    public BigIntCellFormula(CellIndex cell) {
        this.cell = cell;
    }

    @Override
    public boolean isConstant() {
        return false;
    }

    @Override
    public BigInteger calculate(Map<CellIndex, BigInteger> values) {
        BigInteger result = values.get(cell);
        if (result == null) {
            throw new RuntimeException("Calculation Error");
        }

        return result;
    }

    @Override
    public List<Range> getDependencies() {
        return Collections.singletonList(new Range(cell.getRow(), cell.getColumn(), cell.getRow(), cell.getColumn()));
    }
}
