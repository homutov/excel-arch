package homutov.test.formula.bigint;

import homutov.test.formula.Formula;
import homutov.test.formula.FormulaParser;
import homutov.test.formula.Range;
import homutov.test.sheet.CellIndex;

import java.math.BigInteger;

public class BigIntFormulaParser implements FormulaParser<BigInteger> {

    @Override
    public BigInteger nullItem() {
        return BigInteger.ZERO;
    }

    @Override
    public Formula<BigInteger> parse(String value) {
        if (!value.startsWith("=")) {
            try {
                return new BigIntConstantFormula(new BigInteger(value));
            } catch (RuntimeException e) {
                throw new RuntimeException("Parser Error");
            }
        }

        value = value.substring(1);

        Formula<BigInteger> result = parseFormula(value);
        if (result != null) {
            return result;
        }

        throw new RuntimeException("Parse Error");
    }

    private Formula<BigInteger> parseFormula(String value) {
        Formula<BigInteger> result = parseOperation(value);
        if (result != null) {
            return result;
        }

        result = parseRange(value);
        if (result != null) {
            return result;
        }

        result = parseCell(value);
        if (result != null) {
            return result;
        }

        result = parseConstant(value);
        if (result != null) {
            return result;
        }
        return null;
    }

    private Formula<BigInteger> parseConstant(String value) {
        try {
            return new BigIntConstantFormula(new BigInteger(value));
        } catch (RuntimeException e) {
            return null;
        }
    }

    private Formula<BigInteger> parseCell(String value) {
        CellIndex cell = CellIndex.parseCellIndex(value);
        if (cell == null) {
            return null;
        }

        return new BigIntCellFormula(cell);
    }

    private Formula<BigInteger> parseRange(String value) {
        if (!value.startsWith("SUM(") || !value.endsWith(")")) {
            return null;
        }

        value = value.substring(4, value.length() - 1);

        int operationIndex = value.indexOf(":");
        if (operationIndex >= 0) {
            CellIndex left = CellIndex.parseCellIndex(value.substring(0, operationIndex));
            CellIndex right = CellIndex.parseCellIndex(value.substring(operationIndex + 1));
            if (left == null || right == null) {
                return null;
            }

            return new BigIntSumRange(new Range(left.getRow(), left.getColumn(), right.getRow(), right.getColumn()));
        }

        return null;
    }

    private Formula<BigInteger> parseOperation(String value) {
        int operationIndex = Math.max(value.lastIndexOf("+"), value.lastIndexOf("-"));
        if (operationIndex >= 0) {
            char operation = value.charAt(operationIndex);
            Formula<BigInteger> left = parseFormula(value.substring(0, operationIndex));
            Formula<BigInteger> right = parseFormula(value.substring(operationIndex + 1, value.length()));
            if (left == null || right == null) {
                return null;
            }

            if (operation == '+') {
                return new BigIntPlusFormula(left, right);
            } else {
                return new BigIntMinusFormula(left, right);
            }
        }

        return null;
    }
}
