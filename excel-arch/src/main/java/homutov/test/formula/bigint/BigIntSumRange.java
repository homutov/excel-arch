package homutov.test.formula.bigint;

import homutov.test.formula.Formula;
import homutov.test.formula.Range;
import homutov.test.sheet.CellIndex;

import java.math.BigInteger;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class BigIntSumRange implements Formula<BigInteger>{

    private final Range range;

    public BigIntSumRange(Range range) {
        this.range = range;
    }

    @Override
    public boolean isConstant() {
        return false;
    }

    @Override
    public BigInteger calculate(Map<CellIndex, BigInteger> values) {
        BigInteger result = BigInteger.valueOf(0);

        for (long i = range.getTop(); i <= range.getBottom(); i++) {
            for (long j = range.getLeft(); j <= range.getRight(); j++) {
                CellIndex current = new CellIndex(i, j);
                BigInteger value = values.get(current);
                if (value == null) {
                    throw new RuntimeException("Calculation Error");
                }

                result = result.add(value);
            }
        }

        return result;
    }

    @Override
    public List<Range> getDependencies() {
        return Collections.singletonList(range);
    }
}
