package homutov.test.formula.bigint;

import homutov.test.formula.Formula;
import homutov.test.formula.Range;
import homutov.test.sheet.CellIndex;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BigIntMinusFormula implements Formula<BigInteger> {

    private final Formula<BigInteger> left;

    private final Formula<BigInteger> right;

    public BigIntMinusFormula(Formula<BigInteger> left, Formula<BigInteger> right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public boolean isConstant() {
        return false;
    }

    @Override
    public BigInteger calculate(Map<CellIndex, BigInteger> values) {
        return left.calculate(values).subtract(right.calculate(values));
    }

    @Override
    public List<Range> getDependencies() {
        List<Range> result =  new ArrayList<>();

        result.addAll(left.getDependencies());
        result.addAll(right.getDependencies());

        return result;
    }
}
