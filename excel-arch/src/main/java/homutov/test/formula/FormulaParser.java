package homutov.test.formula;

public interface FormulaParser<T> {

    T nullItem();

    Formula<T> parse(String value);
}
