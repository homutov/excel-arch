package homutov.test.formula;

import homutov.test.sheet.CellIndex;

import java.util.List;
import java.util.Map;

public interface Formula<T> {

    boolean isConstant();

    T calculate(Map<CellIndex, T> values);

    List<Range> getDependencies();

}
