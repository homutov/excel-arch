package homutov.test.formula;

import homutov.test.sheet.CellIndex;

public class Range {

    private final long left;
    private final long right;
    private final long top;
    private final long bottom;


    public Range(long top, long left, long bottom, long right) {
        this.left = left;
        this.right = right;
        this.top = top;
        this.bottom = bottom;
    }

    public boolean contains(CellIndex cell) {
        return cell.getRow() >= top && cell.getRow() <= bottom && cell.getColumn() >= left && cell.getColumn() <= right;
    }

    public long getLeft() {
        return left;
    }

    public long getRight() {
        return right;
    }

    public long getTop() {
        return top;
    }

    public long getBottom() {
        return bottom;
    }

    @Override
    public String toString() {
        return "Range{top=" + top + ", left=" + left + ", bottom=" + bottom + ", right=" + right + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Range range = (Range) o;

        if (left != range.left) return false;
        if (right != range.right) return false;
        if (top != range.top) return false;
        return bottom == range.bottom;

    }

    @Override
    public int hashCode() {
        int result = (int) (left ^ (left >>> 32));
        result = 31 * result + (int) (right ^ (right >>> 32));
        result = 31 * result + (int) (top ^ (top >>> 32));
        result = 31 * result + (int) (bottom ^ (bottom >>> 32));
        return result;
    }
}
