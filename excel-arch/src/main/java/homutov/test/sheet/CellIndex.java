package homutov.test.sheet;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CellIndex {

    private final long row;

    private final long column;

    public CellIndex(long row, long column) {
        this.row = row;
        this.column = column;
    }

    public long getRow() {
        return row;
    }

    public long getColumn() {
        return column;
    }

    @Override
    public String toString() {
        return "CellIndex{row=" + row + ", column=" + column + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CellIndex cell = (CellIndex) o;

        if (row != cell.row) return false;
        return column == cell.column;

    }

    @Override
    public int hashCode() {
        int result = (int) (row ^ (row >>> 32));
        result = 31 * result + (int) (column ^ (column >>> 32));
        return result;
    }

    private static final Pattern cellPattern = Pattern.compile("([A-Z]+)([\\d]+)");

    private static long parseRow(String row) {
        long result = 0;

        for (int index = 0; index < row.length(); index++) {
            result = 26 * result + (row.charAt(index) - 'A') + 1;
        }

        return result - 1;
    }

    public static CellIndex parseCellIndex(String value) {
        Matcher matcher = cellPattern.matcher(value);
        if (matcher.matches()) {
            String row = matcher.group(1);
            String column = matcher.group(2);

            try {
                return new CellIndex(
                        CellIndex.parseRow(row),
                        Long.valueOf(column)
                );
            } catch (RuntimeException e) {
                return null;
            }
        }

        return null;
    }
}
