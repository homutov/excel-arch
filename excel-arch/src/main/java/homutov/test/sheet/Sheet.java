package homutov.test.sheet;

import homutov.test.formula.Formula;
import homutov.test.formula.FormulaParser;
import homutov.test.formula.Range;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Consumer;

public class Sheet<T> {

    private final Links links = new Links();

    private final Map<CellIndex, Formula<T>> cells = new HashMap<>();

    private final ReadWriteLock lock = new ReentrantReadWriteLock();

    private final FormulaParser<T> formulaParser;

    public Sheet(FormulaParser<T> formulaParser) {
        this.formulaParser = formulaParser;
    }

    public boolean setValue(long row, long column, String value) {
        lock.writeLock().lock();

        try {
            if (value == null) {
                removeValue(row, column);
                return true;
            }

            Formula<T> formula = formulaParser.parse(value);
            if (formula == null) {
                return false;
            }

            CellIndex cell = new CellIndex(row, column);

            cells.put(cell, formula);

            List<Link> currentLinks = links.removeLinks(cell);

            for (Range range : formula.getDependencies()) {
                Link link = Link.create(cell, range);

                if (!links.addLink(link)) {
                    links.removeLinks(cell);
                    currentLinks.forEach(links::addLink);
                    return false;
                }
            }

            return true;
        } finally {
            lock.writeLock().unlock();
        }
    }

    public Formula<T> getValue(long row, long column) {
        lock.readLock().lock();

        try {
            return cells.get(new CellIndex(row, column));
        } finally {
            lock.readLock().unlock();
        }
    }

    public void removeValue(long row, long column) {
        lock.writeLock().lock();

        try {
            CellIndex cell = new CellIndex(row, column);
            links.removeLinks(cell);
            cells.remove(cell);
        } finally {
            lock.writeLock().unlock();
        }
    }

    public FormulaParser<T> getFormulaParser() {
        return formulaParser;
    }

    public void forEachDependent(CellIndex cell, Consumer<CellIndex> consumer) {
        lock.readLock().lock();

        try {
            links.forEachDependent(cell, consumer);
        } finally {
            lock.readLock().unlock();
        }
    }
}