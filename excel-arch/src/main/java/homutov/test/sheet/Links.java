package homutov.test.sheet;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Links {

    private final List<Link> links = new ArrayList<>();

    private final Map<CellIndex, Set<CellIndex>> index = new HashMap<>();
    private final Map<CellIndex, Set<CellIndex>> revertedIndex = new HashMap<>();

    public boolean addLink(Link link) {
        if (link.getTo().contains(link.getFrom())) {
            return false;
        }

        doAddLink(link);

        if (checkForCycles(link.getFrom(), index)) {
            doRemoveLink(link);
            return false;
        }

        return true;
    }

    private boolean checkForCycles(CellIndex start, Map<CellIndex, Set<CellIndex>> index) {
        class CycleChecker implements Consumer<CellIndex> {

            private boolean hasCycle = false;

            @Override
            public void accept(CellIndex cell) {
                Set<CellIndex> adjacentVertices = index.get(cell);
                hasCycle |= adjacentVertices != null && adjacentVertices.contains(start);
            }
        }

        CycleChecker cycleChecker = new CycleChecker();
        forEach(Collections.singleton(start), index, cycleChecker);

        return cycleChecker.hasCycle;
    }

    public List<Link> removeLinks(CellIndex cell) {
        List<Link> linksToRemove = links.parallelStream()
                .filter(l -> l.getFrom().equals(cell))
                .collect(Collectors.toList());

        linksToRemove.forEach(this::doRemoveLink);

        return linksToRemove;
    }

    private void doAddLink(Link link) {
        links.add(link);
        addToIndexes(link, l -> isAdjacent(link, l), index, revertedIndex);
        addToIndexes(link, l -> isAdjacent(l, link), revertedIndex, index);
    }

    private boolean isAdjacent(Link from, Link to) {
        return from.getTo().contains(to.getFrom());
    }

    private void addToIndexes(
            Link link,
            Predicate<Link> isAdjacent,
            Map<CellIndex, Set<CellIndex>> index,
            Map<CellIndex, Set<CellIndex>> revertedIndex
    ) {
        Set<CellIndex> adjacent = links.parallelStream()
                .filter(isAdjacent)
                .map(Link::getFrom)
                .collect(Collectors.toSet());

        if (!adjacent.isEmpty()) {
            index.put(link.getFrom(), adjacent);

            adjacent.forEach(
                    c -> {
                        Set<CellIndex> dependent = revertedIndex.get(c);
                        if (dependent == null) {
                            dependent = new HashSet<>();
                            revertedIndex.put(c, dependent);
                        }

                        dependent.add(link.getFrom());
                    }
            );
        }
    }

    private void doRemoveLink(Link link) {
        links.remove(link);

        Set<CellIndex> adjacent = index.remove(link.getFrom());
        removeFromIndex(link, adjacent, revertedIndex);

        Set<CellIndex> dependent = revertedIndex.remove(link.getFrom());
        removeFromIndex(link, dependent, index);
    }

    private void removeFromIndex(
            Link link,
            Set<CellIndex> adjacent,
            Map<CellIndex, Set<CellIndex>> revertedIndex
    ) {
        if (adjacent != null) {
            adjacent.forEach(
                    c -> {
                        Set<CellIndex> dependent = revertedIndex.get(c);
                        if (dependent != null) {
                            dependent.remove(link.getFrom());
                            if (dependent.isEmpty()) {
                                revertedIndex.remove(c);
                            }
                        }
                    }
            );
        }
    }

    public void forEachDependent(CellIndex cell, Consumer<CellIndex> consumer) {
        Set<CellIndex> dependent = links.parallelStream()
                .filter(link -> link.getTo().contains(cell))
                .map(Link::getFrom)
                .collect(Collectors.toSet());

        forEach(dependent, revertedIndex, consumer);
    }

    protected <T> void forEach(Collection<T> start, Map<T, Set<T>> index, Consumer<T> cons) {
        Queue<T> queue = new LinkedList<>();
        queue.addAll(start);

        Set<T> visited = new HashSet<>(start);
        while (!queue.isEmpty()) {
            T next = queue.poll();
            Set<T> adjacentVertices = index.get(next);
            if (adjacentVertices != null) {
                List<T> newVertices = adjacentVertices.stream()
                        .filter(v -> !visited.contains(v))
                        .collect(Collectors.toList());

                queue.addAll(newVertices);
                visited.addAll(newVertices);
            }

            cons.accept(next);
        }
    }
}
