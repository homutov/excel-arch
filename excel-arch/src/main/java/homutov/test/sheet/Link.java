package homutov.test.sheet;

import homutov.test.formula.Range;

import java.util.concurrent.atomic.AtomicInteger;

public class Link {

    private static final AtomicInteger idGenerator = new AtomicInteger();

    private final int id;

    private final CellIndex from;

    private final Range to;

    private Link(int id, CellIndex from, Range to) {
        this.id = id;
        this.from = from;
        this.to = to;
    }

    public CellIndex getFrom() {
        return from;
    }

    public Range getTo() {
        return to;
    }

    @Override
    public String toString() {
        return "Link{id=" + id + ", from=" + from + ", to=" + to + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Link link = (Link) o;

        return id == link.id;

    }

    @Override
    public int hashCode() {
        return id;
    }

    public static Link create(CellIndex from, Range to) {
        return new Link(idGenerator.incrementAndGet(), from, to);
    }
}
