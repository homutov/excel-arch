package homutov.test.display;

import homutov.test.sheet.CellIndex;

import java.util.concurrent.ConcurrentHashMap;

public class ValueCache<T> {

    private final ConcurrentHashMap<CellIndex, T> valueCache = new ConcurrentHashMap<>();

    private final boolean enableCache; // for testing

    public ValueCache(boolean enableCache) {
        this.enableCache = enableCache;
    }

    public void put(CellIndex cell, T value) {
        if (enableCache) {
            valueCache.put(cell, value);
        }
    }

    public T get(CellIndex cell) {
        if (enableCache) {
            return valueCache.get(cell);
        }
        return null;
    }

    public void invalidate(CellIndex cell) {
        if (enableCache) {
            valueCache.remove(cell);
        }
    }
}
