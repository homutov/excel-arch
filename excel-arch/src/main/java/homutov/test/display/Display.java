package homutov.test.display;

import homutov.test.formula.Formula;
import homutov.test.formula.Range;
import homutov.test.formula.bigint.BigIntFormulaParser;
import homutov.test.sheet.CellIndex;
import homutov.test.sheet.Sheet;

import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class Display {

    private final Sheet<BigInteger> sheet = new Sheet<>(new BigIntFormulaParser());

    private final ValueCache<BigInteger> valueCache;

    public Display() {
        this(true);
    }

    // For testing
    Display(boolean enableCache) {
        valueCache =  new ValueCache<>(enableCache);
    }

    public synchronized String[][] display(long top, long left, long bottom, long right) {
        List<CellIndex> cells = new ArrayList<>();
        for (long i = top; i <= bottom; i++) {
            for (long j = left; j <= right; j++) {
                cells.add(new CellIndex(i, j));
            }
        }

        Map<CellIndex, String> values = new ConcurrentHashMap<>();
        cells.parallelStream().forEach(
                c -> values.put(c, calculate(c, sheet, valueCache).toString())
        );

        String[][] result = new String[(int) (bottom - top + 1)][(int) (right - left + 1)];
        for (long i = top; i <= bottom; i++) {
            for (long j = left; j <= right; j++) {
                result[(int) (i - top)][(int) (j - left)] = values.get(new CellIndex(i, j));
            }
        }

        return result;
    }

    private static <T> T calculate(CellIndex cell, Sheet<T> sheet, ValueCache<T> valueCache) {
        T cachedValue = valueCache.get(cell);
        if (cachedValue != null) {
            return cachedValue;
        }

        Formula<T> formula = sheet.getValue(cell.getRow(), cell.getColumn());
        if (formula == null) {
            return sheet.getFormulaParser().nullItem();
        }

        Map<CellIndex, T> values = new HashMap<>();
        if (formula.isConstant()) {
            return formula.calculate(values);
        } else {
            for (Range range : formula.getDependencies()) {
                for (long i = range.getTop(); i <= range.getBottom(); i++) {
                    for (long j = range.getLeft(); j <= range.getRight(); j++) {
                        CellIndex current = new CellIndex(i, j);
                        T value = calculate(current, sheet, valueCache);
                        values.put(current, value);
                    }
                }
            }

            T value = formula.calculate(values);
            valueCache.put(cell, value);

            return value;
        }
    }

    public synchronized void setValue(long row, long column, String value) {
        if (!sheet.setValue(row, column, value)) {
            throw new RuntimeException("Incorrect Input Value");
        }

        sheet.forEachDependent(new CellIndex(row, column), valueCache::invalidate);
    }
}
