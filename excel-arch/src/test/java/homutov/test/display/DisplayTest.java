package homutov.test.display;

import homutov.test.sheet.CellIndex;
import org.junit.Assert;
import org.junit.Test;

public class DisplayTest {

    @Test
    public void testSimpleOperations() {
        Display display = new Display();
        display.setValue(0, 0, "100");
        display.setValue(100, 100, "200");
        display.setValue(parseRow("CBQ"), 1000, "300");
        display.setValue(2, 2, "=A0+CW100+CBQ1000+400");
        String[][] result = display.display(0, 0, 2, 2);
        Assert.assertEquals("100", result[0][0]);
        Assert.assertEquals("0", result[0][1]);
        Assert.assertEquals("0", result[0][2]);
        Assert.assertEquals("0", result[1][0]);
        Assert.assertEquals("0", result[1][1]);
        Assert.assertEquals("0", result[1][2]);
        Assert.assertEquals("0", result[2][0]);
        Assert.assertEquals("0", result[2][1]);
        Assert.assertEquals("1000", result[2][2]);
    }

    @Test
    public void testSimpleRange() {
        Display display = new Display();
        display.setValue(5, 5, "5");
        display.setValue(0, 0, "=F5+1");
        display.setValue(0, 1, "=F5+2");
        display.setValue(0, 2, "=F5+3");
        display.setValue(0, 3, "=F5+4");
        display.setValue(1, 0, "=A0+1");
        display.setValue(1, 1, "=A1+2");
        display.setValue(1, 2, "=A2+3");
        display.setValue(1, 3, "=A3+4");
        display.setValue(2, 0, "=B0+1");
        display.setValue(2, 1, "=B1+2");
        display.setValue(2, 2, "=B2+3");
        display.setValue(2, 3, "=B3+4");
        display.setValue(3, 0, "=C0+1");
        display.setValue(3, 1, "=C1+2");
        display.setValue(3, 2, "=C2+3");
        display.setValue(3, 3, "=C3+4");
        display.setValue(4, 0, "=A0+B0+C0+D0");
        display.setValue(4, 1, "=A1+B1+C1+D1");
        display.setValue(4, 2, "=A2+B2+C2+D2");
        display.setValue(4, 3, "=A3+B3+C3+D3");

        String[][] result = display.display(3, 0, 4, 3);
        Assert.assertEquals("9", result[0][0]);
        Assert.assertEquals("13", result[0][1]);
        Assert.assertEquals("17", result[0][2]);
        Assert.assertEquals("21", result[0][3]);
        Assert.assertEquals("30", result[1][0]);
        Assert.assertEquals("40", result[1][1]);
        Assert.assertEquals("50", result[1][2]);
        Assert.assertEquals("60", result[1][3]);
    }

    @Test
    public void testUpdateValue() {
        Display display = new Display();
        display.setValue(5, 5, "5");
        display.setValue(0, 0, "=F5+1");
        display.setValue(0, 1, "=F5+2");
        display.setValue(0, 2, "=F5+3");
        display.setValue(0, 3, "=F5+4");
        display.setValue(1, 0, "=A0+1");
        display.setValue(1, 1, "=A1+2");
        display.setValue(1, 2, "=A2+3");
        display.setValue(1, 3, "=A3+4");
        display.setValue(2, 0, "=B0+1");
        display.setValue(2, 1, "=B1+2");
        display.setValue(2, 2, "=B2+3");
        display.setValue(2, 3, "=B3+4");
        display.setValue(3, 0, "=C0+1");
        display.setValue(3, 1, "=C1+2");
        display.setValue(3, 2, "=C2+3");
        display.setValue(3, 3, "=C3+4");
        display.setValue(4, 0, "=A0+B0+C0+D0");
        display.setValue(4, 1, "=A1+B1+C1+D1");
        display.setValue(4, 2, "=A2+B2+C2+D2");
        display.setValue(4, 3, "=A3+B3+C3+D3");

        String[][] result = display.display(3, 0, 4, 3);
        Assert.assertEquals("9", result[0][0]);
        Assert.assertEquals("13", result[0][1]);
        Assert.assertEquals("17", result[0][2]);
        Assert.assertEquals("21", result[0][3]);
        Assert.assertEquals("30", result[1][0]);
        Assert.assertEquals("40", result[1][1]);
        Assert.assertEquals("50", result[1][2]);
        Assert.assertEquals("60", result[1][3]);

        //Update
        display.setValue(5, 5, "0");

        String[][] result2 = display.display(3, 0, 4, 3);
        Assert.assertEquals("4", result2[0][0]);
        Assert.assertEquals("8", result2[0][1]);
        Assert.assertEquals("12", result2[0][2]);
        Assert.assertEquals("16", result2[0][3]);
        Assert.assertEquals("10", result2[1][0]);
        Assert.assertEquals("20", result2[1][1]);
        Assert.assertEquals("30", result2[1][2]);
        Assert.assertEquals("40", result2[1][3]);
    }

    @Test
    public void testLongChain() {
        Display display = new Display();
        display.setValue(0, 0, "0");

        for (int i = 1; i <= 100; i++) {
            display.setValue(0, i, "=1+A" + (i - 1));
        }

        String[][] result = display.display(0, 100, 0, 100);
        Assert.assertEquals("100", result[0][0]);
    }

    @Test
    public void testLongChainWithUpdate() {
        Display display = new Display();
        display.setValue(0, 0, "0");

        for (int i = 1; i <= 100; i++) {
            display.setValue(0, i, "=1+A" + (i - 1));
        }

        String[][] result = display.display(0, 10, 0, 10);
        Assert.assertEquals("10", result[0][0]);

        display.setValue(0, 0, "10");
        String[][] result2 = display.display(0, 10, 0, 10);
        Assert.assertEquals("20", result2[0][0]);
    }

    @Test
    public void testComplexRange() {
        Display display = new Display();
        display.setValue(0, 0, "1");

        for (int i = 1; i <= 20; i++) {
            display.setValue(0, i, "=SUM(A0:A" + (i - 1) + ")");
        }

        String[][] result = display.display(0, 20, 0, 20);
        Assert.assertEquals("524288", result[0][0]);
    }

    @Test
    public void testComplexRangeWithUpdate() {
        Display display = new Display();
        display.setValue(0, 0, "1");

        for (int i = 1; i <= 20; i++) {
            display.setValue(0, i, "=SUM(A0:A" + (i - 1) + ")");
        }

        String[][] result = display.display(0, 20, 0, 20);
        Assert.assertEquals("524288", result[0][0]);

        // Update
        display.setValue(0, 0, "3");
        String[][] result2 = display.display(0, 20, 0, 20);
        Assert.assertEquals("1572864", result2[0][0]);
    }

    @Test(expected = RuntimeException.class)
    public void testCycleDetection() {
        try {
            Display display = new Display();
            display.setValue(1, 0, "=A0");
            display.setValue(2, 0, "=B0");
            display.setValue(3, 0, "=C0");
            display.setValue(0, 0, "=D0");
        } catch (RuntimeException e) {
            Assert.assertTrue(e.getMessage().startsWith("Incorrect"));
            throw e;
        }
    }

    private static long parseRow(String value) {
        return CellIndex.parseCellIndex(value + "1").getRow();
    }
}
