package homutov.test.sheet;

import homutov.test.formula.Range;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class LinksTest {

    @Test
    public void testFullGraph() {
        Links links = new Links();

        for (int i = 1; i <= 100; i++) {
            links.addLink(Link.create(new CellIndex(i, 0), new Range(0, 0, i - 1, 0)));
        }

        Set<CellIndex> dependent = new HashSet<>();
        links.forEachDependent(new CellIndex(0, 0), dependent::add);

        Assert.assertEquals(100, dependent.size());
    }

    @Test
    public void testSimpleChain() {
        Links links = new Links();

        for (int i = 1; i <= 100; i++) {
            links.addLink(Link.create(new CellIndex(i, 0), new Range(i - 1, 0, i - 1, 0)));
        }

        Set<CellIndex> dependent = new HashSet<>();
        links.forEachDependent(new CellIndex(50, 0), dependent::add);

        Assert.assertEquals(50, dependent.size());
    }

    @Test
    public void testCycleDetection1() {
        Links links = new Links();
        Assert.assertFalse(links.addLink(Link.create(new CellIndex(0, 0), new Range(0, 0, 100, 100))));
    }

    @Test
    public void testCycleDetection2() {
        Links links = new Links();
        Assert.assertTrue(links.addLink(Link.create(new CellIndex(0, 0), new Range(1, 0, 1, 0))));
        Assert.assertFalse(links.addLink(Link.create(new CellIndex(1, 0), new Range(0, 0, 0, 0))));
    }

    @Test
    public void testCycleDetection100() {
        Links links = new Links();

        for (int i = 0; i < 100; i++) {
            if (i > 0) {
                links.addLink(Link.create(new CellIndex(i, 0), new Range(i - 1, 0, i - 1, 0)));
            }
        }

        Assert.assertFalse(links.addLink(Link.create(new CellIndex(0, 0), new Range(50, 0, 50, 0))));
        Assert.assertFalse(links.addLink(Link.create(new CellIndex(0, 0), new Range(99, 0, 99, 0))));
    }

    @Test
    public void testCycleDetectionOnUpdate() {
        Links links = new Links();

        for (int i = 0; i < 10; i++) {
            if (i > 0) {
                links.addLink(Link.create(new CellIndex(i, 0), new Range(i - 1, 0, i - 1, 0)));
            }
        }

        Assert.assertTrue(links.addLink(Link.create(new CellIndex(5, 5), new Range(9, 0, 9, 0))));
        links.removeLinks(new CellIndex(5, 0));
        Assert.assertFalse(links.addLink(Link.create(new CellIndex(5, 0), new Range(5, 5, 5, 5))));
    }
}
