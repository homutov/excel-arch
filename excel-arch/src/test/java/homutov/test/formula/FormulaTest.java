package homutov.test.formula;

import homutov.test.formula.bigint.BigIntFormulaParser;
import homutov.test.sheet.CellIndex;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigInteger;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class FormulaTest {

    private final BigIntFormulaParser parser = new BigIntFormulaParser();

    @Test
    public void testAddition() {
        Formula<BigInteger> addition = parser.parse("=700+1000");
        Assert.assertEquals(BigInteger.valueOf(1700), addition.calculate(Collections.emptyMap()));
    }

    @Test
    public void testSubtraction() {
        Formula<BigInteger> addition = parser.parse("=700-1000");
        Assert.assertEquals(BigInteger.valueOf(-300), addition.calculate(Collections.emptyMap()));
    }

    @Test
    public void testOperations() {
        Formula<BigInteger> addition = parser.parse("=700-1000+100-15-15-15-15+10+110");
        Assert.assertEquals(BigInteger.valueOf(-140), addition.calculate(Collections.emptyMap()));
    }

    @Test
    public void testOperationsWithCell() {
        Formula<BigInteger> addition = parser.parse("=700-A0+100+B0");
        Map<CellIndex, BigInteger> values = new HashMap<>();
        values.put(new CellIndex(0, 0), BigInteger.valueOf(300));
        values.put(new CellIndex(1, 0), BigInteger.valueOf(-400));
        Assert.assertEquals(BigInteger.valueOf(100), addition.calculate(values));
    }

    @Test
    public void testComplexOperations() {
        Formula<BigInteger> addition = parser.parse("=700-A0+100+B0+C2-CW100-CX101+50");
        Map<CellIndex, BigInteger> values = new HashMap<>();
        values.put(new CellIndex(0, 0), BigInteger.valueOf(300));
        values.put(new CellIndex(1, 0), BigInteger.valueOf(-400));
        values.put(new CellIndex(2, 2), BigInteger.valueOf(45));
        values.put(new CellIndex(100, 100), BigInteger.valueOf(1000));
        values.put(new CellIndex(101, 101), BigInteger.valueOf(-500));
        Assert.assertEquals(BigInteger.valueOf(-305), addition.calculate(values));
    }

    @Test
    public void testRange() {
        Formula<BigInteger> addition = parser.parse("=SUM(A0:C1)+10");
        Map<CellIndex, BigInteger> values = new HashMap<>();
        values.put(new CellIndex(0, 0), BigInteger.valueOf(1));
        values.put(new CellIndex(0, 1), BigInteger.valueOf(2));
        values.put(new CellIndex(1, 0), BigInteger.valueOf(3));
        values.put(new CellIndex(1, 1), BigInteger.valueOf(4));
        values.put(new CellIndex(2, 0), BigInteger.valueOf(5));
        values.put(new CellIndex(2, 1), BigInteger.valueOf(6));
        Assert.assertEquals(BigInteger.valueOf(31), addition.calculate(values));
    }

    @Test
    public void testComplexOperationsWithRange() {
        Formula<BigInteger> addition = parser.parse("=SUM(A0:C1)+10-SUM(A0:B1)-SUM(A0:C0)");
        Map<CellIndex, BigInteger> values = new HashMap<>();
        values.put(new CellIndex(0, 0), BigInteger.valueOf(1));
        values.put(new CellIndex(0, 1), BigInteger.valueOf(2));
        values.put(new CellIndex(1, 0), BigInteger.valueOf(3));
        values.put(new CellIndex(1, 1), BigInteger.valueOf(4));
        values.put(new CellIndex(2, 0), BigInteger.valueOf(5));
        values.put(new CellIndex(2, 1), BigInteger.valueOf(6));
        Assert.assertEquals(BigInteger.valueOf(12), addition.calculate(values));
    }

    @Test(expected = RuntimeException.class)
    public void testParseException() {
        try {
            parser.parse("=0,0:2,1+10A");
        } catch (RuntimeException e) {
            Assert.assertTrue(e.getMessage().startsWith("Parse"));
            throw e;
        }
    }

    @Test(expected = RuntimeException.class)
    public void testCalculationError() {
        try {
            Formula<BigInteger> addition = parser.parse("=SUM(A0:C1)+10");
            Map<CellIndex, BigInteger> values = new HashMap<>();
            values.put(new CellIndex(0, 0), BigInteger.valueOf(1));
            values.put(new CellIndex(0, 1), BigInteger.valueOf(2));
            values.put(new CellIndex(1, 0), BigInteger.valueOf(3));
            values.put(new CellIndex(1, 1), BigInteger.valueOf(4));
            values.put(new CellIndex(2, 0), BigInteger.valueOf(5));
            Assert.assertEquals(BigInteger.valueOf(31), addition.calculate(values));
        } catch (RuntimeException e) {
            Assert.assertTrue(e.getMessage().startsWith("Calculation"));
            throw e;
        }
    }
}
